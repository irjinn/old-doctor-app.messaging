'use strict';
const path = require('path');
const fs = require('fs');
const merge = require('merge');
const localConfigFile = path.join(process.cwd(), 'local.config.json');

const localConfig = readLocalConfig();

const config = {
  server:{
    port: 8080,
    indexHtml: null
  },
  db:{
    connectionString: 'mongodb://localhost:27017/message-hub'
  }
};
module.exports = merge.recursive(config, localConfig);
// console.dir(module.exports);

function readLocalConfig(){
  if(!fs.existsSync(localConfigFile)){
    return {};
  }
  console.log('local config: ' + localConfigFile);

  let content = fs.readFileSync(localConfigFile, 'utf8');
  let config = JSON.parse(content);
  // console.dir(config);
  return config;
}