'use strict';
const mongoUrl = require('../../config/index').db.connectionString;
const mongo = require('./mongo');
const fake = require('./fake');

module.exports = {
  initialize(){
    return mongo.initialize(mongoUrl);
  },
  mongo,
  fake
};
