'use strict';
const fakes = require('../fakes');
const clone = require('../utils').clone;

module.exports = {
  initialize(){
    return this;
  },
  contacts(careUnitId){
    return Promise.resolve(fakes.contacts);
  },
  messages(userId){
    return Promise.resolve(fakes.messages);
  },
  conversations(userId){
    return Promise.resolve(fakes.conversations)
      .then(clone)
      .then(removeMessages)
      .then(x => {
        x.forEach(conv => setUser(conv, userId));
        return x;
      });
  },
  conversation(id, userId){
    return Promise.resolve(fakes.conversations)
      .then(arr => arr.find(x => x.id == id))
      .then(clone)
      .then(x => setUser(x, userId));
  }
};

function removeMessages(conversations){
  conversations.forEach(conv => delete conv.messages);
  return conversations;
}

function setUser(conversation, userId){
  if(conversation.latestMessage){
    conversation.latestMessage.toId = userId;
  }
  if(conversation.messages){
    conversation.messages.forEach(x => x.toId = userId);
  }
  return conversation;
}

