'use strict';
let names = module.exports = {
  messages: 'messages',
  clients: 'clients',
  contacts: 'contacts',
  conversations: 'conversations',
  notifications: 'notifications',

  all(){
    return Object.keys(names)
      .map(x => names[x])
      .filter(x => typeof x === 'string');
  },
};
