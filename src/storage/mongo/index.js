'use strict';
const database = require('./database');

module.exports = {
  initialize(url){
    return database.initialize(url)
      .then(() => console.log('connected to mongo: ' + url))
      .catch(err => {
        console.log(`can't connect to ${url}`);
        throw err;
      })
      .then(() => this);
  },
  clients: require('./queries/clients'),
  contacts: require('./queries/contacts'),
  conversations: require('./queries/conversations'),
  messages: require('./queries/messages'),
  database,
};

