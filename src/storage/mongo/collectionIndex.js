'use strict';
const names = require('./collectionNames');

module.exports = {
  create(db) {
    return Promise.resolve(db)
      .then(db => index(db, names.contacts, 'id'))
      .then(db => index(db, names.clients, 'clientId'))
      .then(db => index(db, names.messages, 'id'))
      .then(db => index(db, names.messages, 'conversationId'))
      .then(db => index(db, names.conversations, 'doctorId'))
      .then(db => index(db, names.conversations, 'patientId'))
      ;
  }
};

function index(db, collectionName, field) {
  return db.collection(collectionName)
    .createIndex(field)
    .then(() => db);
}