'use strict';
const mongo = require('mongodb');
const guid = require('../../utils/guid');
const index = require('./collectionIndex');
const collectionNames = require('./collectionNames');

var _db = null;
function db(){
  if(!_db){
    throw new Error('mongo database is not initialized');
  }
  return Promise.resolve(_db);
}
function collection(name){
  return db()
    .then(db => db.collection(name));
}
module.exports = {
  initialize(url){
    return mongo.MongoClient.connect(url)
      .then(db => index.create(db))
      .then(db => _db = db);
  },
  collection,
  deleteAllData(){
    return Promise.all(collectionNames.all()
      .map(name =>
        collection(name).then(x => x.remove({}))));
  },
  getById(id, collectionName){
    if(!id){
      return Promise.resolve(null);
    }
    let isMongoId = mongo.ObjectId.isValid(id);
    let query = isMongoId
      ? { _id: { $eq: mongo.ObjectId(id) }}
      : { id: { $eq: id }};

    return collection(collectionName)
      .then(x => x.findOne(query));
  },
};
