'use strict';
const db = require('../database');
const names = require('../collectionNames');
const collection = () => db.collection(names.messages);
const ObjectId = require('mongodb').ObjectId;

module.exports = { //messages
  findById(id){
    return db.getById(id, names.messages);
  },
  insert(msg){
    return collection()
      .then(x => x.insertOne(msg));
  },
  setIsRead(id){
    let query = { _id: { $eq: id }};

    return collection()
      .then(x => x
        .update(query, { $set: { isRead: true } }, { upsert: true }));
  },
  //todo: join messages with clients on msg.fromId = client.id
  fromConversation(conversationId){
    let aggr = [
      { $match: { conversationId: conversationId } },
      { $sort: { _id: 1 }},
      { $lookup: {
        from: names.contacts,
        foreignField: 'id',
        localField: 'fromId',
        as: 'from',
      }},
    ];
    return collection()
      .then(x => x.aggregate(aggr))
      .then(x => x.toArray())
      .then(arr => {
        arr.forEach(x => {
          x.from = x.from ? x.from[0] : null;
        });
        return arr;
      });
  }
};