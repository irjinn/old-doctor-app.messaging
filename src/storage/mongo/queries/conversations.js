'use strict';
const db = require('../database');
const names = require('../collectionNames');
const collection = () => db.collection(names.conversations);

function insert(conv){
  return collection()
    .then(x => x.insertOne(conv));
}
function upsert(conv){
  let query = {
    $or: [
      { _id: { $eq: conv._id } },
      { id: { $eq: conv.id } },
    ]};
  return collection()
    .then(x => x.update(query, conv, { upsert: true }));
}
function findById(id){
  return db.getById(id, names.conversations);
}
function findByDoctorAndPatient(doctorId, patientId){
  let query = {
    $and: [
      { doctorId: { $eq: doctorId } },
      { patientId: { $eq: patientId } },
    ]};
  return collection()
    .then(x => x.findOne(query));
}
function findOne(conversation){
  let convId = conversation._id || conversation.id;
  let doctorId = conversation.doctorId;
  let patientId = conversation.patientId;

  return db.getById(convId, names.conversations)
    .then(conv => conv ||
      findByDoctorAndPatient(doctorId, patientId));
}

const patient = {
  all(patientId, query){
    let aggr = [
      { $match: { patientId: patientId } },
      { $lookup: {
        from: names.contacts,
        foreignField: 'id',
        localField: 'doctorId',
        as: 'contact',
      }},
    ];
    return this.aggregate(aggr);
  },
  details(conversationId){
    let aggr = [
      { $match: { id: conversationId } },
      { $lookup: {
        from: names.contacts,
        foreignField: 'id',
        localField: 'doctorId',
        as: 'contact',
      }},
    ];
    return this.aggregate(aggr)
      .then(arr => arr[0]);
  },
  aggregate(query){
    return collection()
      .then(x => x.aggregate(query))
      .then(x => x.toArray())
      .then(arr => {
        arr.forEach(x => {
          x.contact = x.contact ? x.contact[0] : null;
        });
        return arr;
      });
  }
};
module.exports = { //conversations
  insert,
  upsert,
  // details,
  findById,
  findByDoctorAndPatient,
  findOne,
  patient,
  // doctor,
};
