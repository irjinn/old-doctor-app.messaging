'use strict';
const db = require('../database');
const names = require('../collectionNames');
const collection = () => db.collection(names.clients);

module.exports = { //clients
  all(careUnitId){
    return collection()
      .then(x => x.find({}).toArray());
  },
  insert(careUnitId, contact){
    return collection()
      .then(x => x.insertOne(contact))
      .then(() => contact);
  },
  upsert(client){
    var query = { clientId: { $eq: client.clientId }};
    return collection()
      .then(x => x.update(query, { $set: client }, { upsert: true }))
      .then(() => client);
  }
};