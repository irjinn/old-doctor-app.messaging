'use strict';
const db = require('../database');
const names = require('../collectionNames');
const collection = () => db.collection(names.contacts);

module.exports = { //contacts
  all(){
    //next: filter contacts on careUnitId
    return collection()
      .then(x => x.find({}).toArray());
  },
  getById(id){
    return db.getById(id, names.contacts);
  },
  insert(contact){
    //next: insert or update contacts array
    return collection()
      .then(x => x.insertOne(contact));
  },
  deleteAll(){
    return collection()
      .then(x => x.remove({}));
  },
};