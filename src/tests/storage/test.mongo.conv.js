const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const describe = require('mocha').describe;
const before = require('mocha').before;
const utils = require('../../utils');
const env = require('../env');
chai.use(chaiAsPromised);
chai.should();

//mongo
const mongo = require('../../storage/mongo');
const storage = mongo.conversations;

//models
const conversation = require('../../models/conversation');
const contact = require('../../models/contact');

describe('mongo.conversations', function() {
  before(env.cleanDb);

  describe('upsert(conv)', function() {
    it('creates conversation if not exists', function() {
      let expected = conversation.fake();
      return storage.upsert(expected)
        .then(cr => storage.findOne(expected))
        .then(utils.sanitize.model)
        .should.eventually.deep.equal(expected)        ;
    });
  });

  describe('findOne(conv)', function() {
    it('can find conversation by id', function() {
      let expected = conversation.fake();
      return storage.insert(expected)
        .then(cr => storage.findOne({ id: expected.id }))
        .should.eventually.deep.equal(expected);
    });

    it('can find conversation by doctorId/patientId', function() {
      let expected = conversation.fake();
      let query = {
        doctorId: expected.doctorId,
        patientId: expected.patientId
      };
      return storage.insert(expected)
        .then(cr => storage.findOne(query))
        .should.eventually.deep.equal(expected);
    });
  });
});
