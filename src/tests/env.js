const mongo = require('../storage/mongo');
const config = require('./test.config.json');

var mongoInitialized = false;

function initializeDb() {
  if(!mongoInitialized) {
    mongoInitialized = true;
    return mongo.initialize(config.mongo.connectionString);
  }
  return Promise.resolve(mongo);
}

module.exports = {
  initDb: initializeDb,
  cleanDb(){
    return initializeDb()
      .then(storage => storage.database.deleteAllData());
  },
};
