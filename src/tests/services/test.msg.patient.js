const chai = require('chai');
const describe = require('mocha').describe;
const before = require('mocha').before;
const env = require('../env');
const expect = chai.expect;

//services
const messages = require('../../services/messages');
const notifications = require('../../socketServer/rx');
const conversations = require('../../services/conversations');

//models
const conversation = require('../../models/conversation');
const message = require('../../models/message');
const contact = require('../../models/contact');
const notificationType = require('../../models/notificationType');


describe('messages.patient', function() {
  before(env.cleanDb);

  describe('add(msg)', function() {
    it('creates correct conversation if not exists', function() {
      let expMsg = message.fake();
      let doctorId = expMsg.toId;
      let patientId = expMsg.fromId;
      return messages.patient.add(expMsg)
        .then(() => conversations.patient.all(patientId))
        .then(arr => {
          let conv = arr.find(x =>
            x.doctorId == doctorId && x.patientId == patientId);

          expect(conv).to.not.equal(null, 'cannot find conversation');
          expect(conv.doctorId).to.equal(doctorId);
          expect(conv.patientId).to.equal(patientId);
          expect(conv.lastMessage).to.deep.equal(expMsg);
        });
    });

    it('triggers socket notification', function() {
      this.timeout(500);
      let expMsg = message.fake();
      let patientId = expMsg.fromId;
      let awaitNotification = notifications.queue
        .first(x => x.type === notificationType.message)
        .toPromise();

      return messages.patient.add(expMsg)
        .then(() => awaitNotification)
        .then(ntf => {
          expect(ntf.fromId).to.equal(patientId);
          expect(ntf.data).to.deep.equal(expMsg);
        });
    });
  });
});
