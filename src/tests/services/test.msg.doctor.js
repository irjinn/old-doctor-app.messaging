const chai = require('chai');
const describe = require('mocha').describe;
const before = require('mocha').before;
const env = require('../env');
const utils = require('../../utils');
const expect = chai.expect;

//services
const messages = require('../../services/messages');
const contacts = require('../../services/contacts');
const notifications = require('../../socketServer/rx');
const conversations = require('../../services/conversations');

//models
const conversation = require('../../models/conversation');
const message = require('../../models/message');
const contact = require('../../models/contact');
const notificationType = require('../../models/notificationType');


describe('messages.doctor', function() {
  before(env.cleanDb);

  describe('add(msg)', function() {
    it('creates correct conversation if not exists', function() {
      let expMsg = message.fake();
      let doctorId = expMsg.fromId;
      let patientId = expMsg.toId;
      return messages.doctor.add(expMsg)
        .then(() => conversations.patient.all(patientId))
        .then(arr => {
          let conv = arr.find(x =>
          x.doctorId == doctorId && x.patientId == patientId);

          expect(conv).to.not.equal(null, 'cannot find conversation');
          expect(conv.doctorId).to.equal(doctorId);
          expect(conv.patientId).to.equal(patientId);
          expect(conv.lastMessage).to.deep.equal(expMsg);
        });
    });

    it('triggers socket notification, embed doctor contact', function() {
      this.timeout(500);
      let doctorContact = contact.fake({ type: 'doctor' });
      let expMsg = message.fake({ fromId: doctorContact.id });

      let awaitNotification = notifications.queue
        .first(x => x.type === notificationType.message)
        .toPromise();

      return contacts.add(utils.clone(doctorContact))
        .then(() => messages.doctor.add(expMsg))
        .then(() => awaitNotification)
        .then(ntf => {
          expect(ntf.fromId).to.equal(doctorContact.id);
          expect(ntf.data).to.not.equal(null, 'notification contains no data');
          expect(ntf.data.from).to.deep.equal(doctorContact);
        });
    });
  });
});
