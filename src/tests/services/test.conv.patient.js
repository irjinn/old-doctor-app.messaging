const chai = require('chai');
const describe = require('mocha').describe;
const before = require('mocha').before;
const env = require('../env');
const expect = chai.expect;

//services
const mongo = require('../../storage/mongo');
const messages = require('../../services/messages');
const conversations = require('../../services/conversations');
const contacts = require('../../services/contacts');

//models
const conversation = require('../../models/conversation');
const message = require('../../models/message');
const contact = require('../../models/contact');
const notificationType = require('../../models/notificationType');


describe('conversations.patient', function() {
  before(env.cleanDb);

  describe('details(convId)', function () {
    it('returns conversation with doctor contact in messages', function () {
      let doctorContact = contact.fake({ type: 'doctor' });
      let patientContact = contact.fake({ type: 'patient' });

      let doctorId = doctorContact.id;
      let patientId = patientContact.id;

      let patientMsg = message.fake({
        toId: doctorId,
        fromId: patientId,
      });
      let doctorMsg = message.fake({
        toId: patientId,
        fromId: doctorId,
      });

      return contacts.add(doctorContact)
        .then(() => contacts.add(patientContact))
        .then(() => messages.patient.add(patientMsg))
        .then(() => messages.doctor.add(doctorMsg))
        .then(() => conversations.patient.all(patientId))
        .then(arr => arr[0])
        .then(conv => conversations.patient.details(conv.id))
        .then(conv => {
          expect(conv).to.not.equal(null, 'conversation is not found');
          expect(conv.messages).to.have.lengthOf(2, 'messages count is wrong');
          var docMsg = conv.messages.find(x => x.fromId == doctorId);
          var pacMsg = conv.messages.find(x => x.fromId == patientId);
          expect(docMsg.from).to.deep.equal(doctorContact);
          expect(pacMsg.from).to.deep.equal(patientContact);
        });
    });
  });

  describe('all(patientId)', function () {
    it('returns conversations with embedded doctor contact', function () {
      let doctorContact = contact.fake();
      let msg = message.fake({ toId: doctorContact.id });
      let patientId = msg.fromId;

      return contacts.add(doctorContact)
        .then(() => messages.patient.add(msg))
        .then(() => conversations.patient.all(patientId))
        .then(arr => {
          let conv = arr.pop();
          expect(conv).to.not.equal(null, 'conversation is not found');
          expect(conv.contact).to.deep.equal(doctorContact);
        });
    });
  });
});
