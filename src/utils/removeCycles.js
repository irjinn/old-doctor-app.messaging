'use strict';
const json = require('circular-json');

module.exports = function (obj){
  if(!obj){
    return obj;
  }
  let content = json.stringify(obj);
  return JSON.parse(content);
};
