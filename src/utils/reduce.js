'use strict';
const clone = require('./clone');
module.exports = {
  property(obj, propertyName){
    if(!obj){
      return obj;
    }
    var res = clone(obj);
    delete res[propertyName];
    return res;
  }
};
