'use strict';
const json = require('circular-json');
module.exports = function (obj) {
  return json.parse(json.stringify(obj));
};
