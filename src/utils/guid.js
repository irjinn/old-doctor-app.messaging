const validationRegex = /^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$/i;
module.exports = {
  new(){
    return(""+1e7+-1e3+-4e3+-8e3+-1e11)
      .replace(/1|0/g, () => (0|Math.random()*16).toString(16));
  },
  validate(guid){
    return guid && validationRegex.test(guid);
  }
};