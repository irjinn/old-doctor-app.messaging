'use strict';
const reduce = require('./reduce');
module.exports = {
  model(obj){
    return Array.isArray(obj)
      ? obj.map(sanitizeModel)
      : sanitizeModel(obj);
  }
};
//m.b. sanitize nested objects optionally?
function sanitizeModel(obj){
  if(!obj){
    return obj;
  }
  return reduce.property(obj, '_id');
}