'use strict';

module.exports = function(date){
  date = date || new Date();
  let year = pad(date.getUTCFullYear());
  let month = pad(date.getUTCMonth());
  let day = pad(date.getUTCDay());
  let hour = pad(date.getUTCHours());
  let minute = pad(date.getUTCMinutes());
  let seconds = pad(date.getUTCSeconds());
  let milliseconds = date.getUTCMilliseconds();
  return `${hour}:${minute}:${seconds}.${milliseconds}`;
};

function pad(num){
  var str = num.toString();
  return str.length >= 2 ? str : '0' + str;
}
