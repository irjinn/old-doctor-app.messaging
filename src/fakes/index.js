'use strict';

module.exports = {
  messages: require('./messages.json'),
  conversations: require('./conversations.json'),
  contacts: require('./contact-list.json'),
};
