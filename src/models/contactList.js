'use strict';
const contact = require('./contact');

module.exports = {
  fake(size){
    var contacts = [];
    size = size || 15;
    for (var i = 0; i < size; i++) {
      contacts.push(contact.fake());
    }
    return [contact.fakeCareUnit()].concat(contacts);
  }
};