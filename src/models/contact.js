'use strict';
const guid = require('../utils/guid');
const fake = require('faker');

module.exports = {
  fake(contact){
    let res = {
      id: guid.new(),
      type: "doctor",
      name: fake.name.findName(),
      title: fakeTitle(),
      avatar: fake.internet.avatar(),
      description: fake.lorem.sentence(),
    };
    return Object.assign(res, contact);
  },
  fakeCareUnit(contact){
    let res = {
      id: '11111111-0000-2222-8888-333333333333', //guid.new(),
      type: 'careunit',
      name: 'Stockholm care',
      title: 'Ordinary clinic',
      avatar: fake.internet.avatar(),
      description: 'Write to the care unit directly.',
    };
    return Object.assign(res, contact);
  },
};

function fakeTitle(){
  return fake.random.arrayElement([
    'Surgeon',
    'Pediatrician',
    'Psychotherapist',
    'Gastroenterologist'
  ]);
}