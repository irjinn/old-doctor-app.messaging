'use strict';
const guid = require('../utils/guid');
const fake = require('faker');

module.exports = {
  create(toId, fromId, body){
    return {
      id:      guid.new(),
      toId:    toId,
      fromId:  fromId,
      body: body,
      // from: { id: '', name: '', avatar: ''},
      // auth: 'sender auth token';
      created: new Date(),
      isRead: false,
    };
  },
  createFrom(msg){
    return {
      id: msg.id || guid.new(),
      toId: msg.toId,
      fromId: msg.fromId,
      body: msg.body,
      conversationId: msg.conversationId,
      // from: { id: '', name: '', avatar: ''},
      // auth: 'sender auth token';
      created: msg.created || new Date(),
      isRead: false,
    }
  },
  fake(msg){
    let res = {
      id: guid.new(),
      toId: guid.new(),
      fromId: guid.new(),
      body: fake.lorem.paragraph(),
      // from: { id: '', name: '', avatar: ''},
      //auth: 'sender security token',  //sender user auth token
      created: fake.date.recent(),
      isRead: false,
    };
    return Object.assign(res, msg);
  }
};
