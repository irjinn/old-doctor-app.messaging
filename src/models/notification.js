'use strict';
const guid = require('../utils/guid');
const clone = require('../utils/clone');
const notificationType = require('./notificationType');
const fake = require('faker');

const notification = module.exports = { //notification
  create(toId, fromId, type, data){
    return {
      id:      guid.new(),
      toId,
      fromId,
      type,
      data,
      created: new Date(),
      // channel: '[optional] msg will be emitted only on special channel',
    };
  },
  fake(ntf){
    let res = {
      id: guid.new(),
      toId: guid.new(),
      fromId: guid.new(),
      data: {},
      // from: { id: '', name: '', avatar: ''},
      //auth: 'sender security token',  //sender user auth token
      created: fake.date.recent(),
    };
    if(ntf && ntf.data){

    }
  },
  isMessage(ntf){
    return ntf.type === notificationType.message;
  },
  fromMessage(msg){
    return notification.create(
      msg.toId,
      msg.fromId,
      notificationType.message,
      msg);
  },
  fromClientStatus(status){
    return notification.create(
      null, //to
      null, //from
      notificationType.clientStatus,
      {
        id: status.clientId,
        name: status.clientName,
        status: status.status
      });
  },
};