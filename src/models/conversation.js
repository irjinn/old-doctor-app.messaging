'use strict';
const guid = require('../utils/guid');

module.exports = {
  create(patientId, doctorId, msg){
    return {
      id: guid.new(),
      patientId,
      doctorId,
      // lastMessageId: msg ? msg.id : null,
      lastMessage: msg,
    };
  },
  createFrom(conv){
    let msg = conv.lastMessage;
    return {
      id: conv.id || guid.new(),
      patientId: conv.patientId,
      doctorId: conv.doctorId,
      // lastMessageId: msg ? msg.id : null,
      lastMessage: msg,
    }
  },
  fake(conv){
    let res = {
      id: guid.new(),
      patientId: guid.new(),
      doctorId: guid.new(),
    };
    return Object.assign(res, conv);
  }
};
