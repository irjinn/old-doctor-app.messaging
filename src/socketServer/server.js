'use strict';
const socketIO = require('socket.io');
const rx = require('./rx');
const clone = require('../utils').clone;
const notification = require('../models').notification;
const clientInfo = require('./clientInfo');
const clientStatus = clientInfo.status;

module.exports = {
  init: attachToHttpServer,
  activeClients: () => Array.from(clientById.values())
};

const clientBySocketId = new Map();
const clientById = new Map();
function onClientConnected(socket){
  let info = clientInfo.fromSocket(socket, clientStatus.online);

  clientBySocketId.set(socket.id, info);
  clientById.set(info.clientId, info);

  rx.clientStatus.onNext(info);
}
function onClientDisconnected(socket){
  let oldInfo = clientBySocketId.get(socket.id);
  let clientInfo = Object.assign(
    clone(oldInfo),
    { status: clientStatus.offline });

  clientBySocketId.delete(socket.id);
  clientById.delete(oldInfo.clientId);

  rx.clientStatus.onNext(clientInfo);
}
function onClientEvent(socket, event){
  let clientInfo = clientBySocketId.get(socket.id);
  event = clone(event);
  event.from = clientInfo.clientId;
  rx.clientEvents.onNext(event);
}

function attachToHttpServer(server){
  const io = module.exports.io = socketIO(server, {});

  io.on('connection', (socket) => {
    socket.on('disconnect-me', () => {
      socket.disconnect();
    });

    socket.on('disconnect', () => {
      onClientDisconnected(socket);
    });

    socket.on('join', channel => {
      if(typeof channel === 'string' && channel){
        socket.join(channel);
      }
    });
    socket.on('leave', channel => {
      if(typeof channel === 'string' && channel){
        socket.leave(channel);
      }
    });

    socket.on('notification', event => {
      onClientEvent(socket, event);
    });

    // syntactic sugar for notification with type = message
    socket.on('message', msg => {
      let event = notification.fromMessage(msg);
      onClientEvent(socket, event);
    });

    onClientConnected(socket);
  });
  console.log('socket server initialized');
  subscribeToNotifications(io);
}

function subscribeToNotifications(io){
  let broadcastQueue = rx.queue.filter(x => !x.to);
  let targetedQueue = rx.queue.filter(x => x.to);

  broadcastQueue
    .filter(x => x.channel)
    .subscribe(x => io.to(x.channel).emit('notification', x));

  broadcastQueue
    .filter(x => !x.channel)
    .subscribe(x => io.emit('notification', x));

  targetedQueue
    .subscribe(x => {
      //todo: emit diagnostics event {success, eventId, senderId, receiverId}
      let userInfo = clientById.get(x.to);
      if(!userInfo){
        return; //if no one to receive
      }
      let socketId = userInfo.socketId;
      let sockets = (io.sockets || {}).sockets || {};
      let socket = sockets[socketId];
      if(socket){ //should always be true
        socket.emit('notification', x);
      }
    });
}



