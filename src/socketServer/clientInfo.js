'use strict';
const statusType = { //socket client status type
  online: 'online',
  offline: 'offline'
};

// socket connection specific info
module.exports = {
  status: statusType,
  fromSocket(socket, status){
    let conn = (socket.client || {}).conn;
    return {
      clientId: socket.handshake.query.id,
      socketId: socket.id,
      clientName: socket.handshake.query.name,
      query: socket.handshake.query,
      status: status,
      ip: conn.remoteAddress,
    };
  }
};

