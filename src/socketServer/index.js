'use strict';
const rx = require('./rx');
const server = require('./server');
const router = require('./router');
require('./routes'); //initialize routes

module.exports = {
  init: server.init,
  activeClients: server.activeClients,
  postTextMessage: router.postTextMessage,
  postNotification: router.postNotification,
};



