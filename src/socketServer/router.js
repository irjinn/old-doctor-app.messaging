'use strict';
const notification = require('../models').notification;
const rx = require('./rx');

//external interface
module.exports = {
  postTextMessage(msg){
    let ntf = notification.fromMessage(msg);
    ntf.to = null; //test broadcast
    ntf.to = ntf.from; //echo to sender
    this.postNotification(ntf);
    return msg;
  },
  postNotification(notification){
    rx.queue.onNext(notification);
    return notification;
  },
};



