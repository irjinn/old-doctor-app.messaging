'use strict';
const notification = require('../../models').notification;
const router = require('../router');
const rx = require('../rx');

// configure default event flows

rx.clientEvents // echo message back
  .filter(x => notification.isMessage(x))
  .do(x => x.toId = x.fromId) //echo to sender
  .subscribe(router.postNotification);

//todo: investigate scope issue
rx.clientStatus // notify all clients on client state change
  .do(x => require('../../services/clients').update(x))
  .map(notification.fromClientStatus)
  .subscribe(router.postNotification);