'use strict';
const Rx = require('rx');

//rx adapter for socket server
module.exports = {
  queue: new Rx.Subject(),        // notifications to clients
  clientEvents: new Rx.Subject(), // events from clients
  clientStatus: new Rx.Subject()  // events from connect/disconnect status
};
