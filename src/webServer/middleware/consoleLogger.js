'use strict';
const timeStamp = require('../../utils/index').timeStamp;

module.exports = function (req, res, next) {
  let url = `${req.headers.host}${req.originalUrl}`;
  let message = `[${timeStamp()}] ${req.method}: ${url}`;
  console.log(message);
  next();
};