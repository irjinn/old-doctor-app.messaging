'use strict';

module.exports = function (req, res, next) {
  res.jsonSuccess = data => res.json({ success: true, data });
  res.jsonError = err => res.json({ success: false, error: err });
  next();
};
