'use strict';
const storage = require('../../storage').mongo;
module.exports = function (req, res, next) {
  res.storage = storage;
  next();
};

