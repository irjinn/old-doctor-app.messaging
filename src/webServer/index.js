'use strict';
const fs = require('fs');
const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const router = require('./routes');
const config = require('../../config');
const consoleLogger = require('./middleware/consoleLogger');
const extensions = require('./middleware/extensions');
const storage = require('./middleware/storage');

module.exports = class WebFacade{
  get app(){
    return this._app;
  }
  get server(){
    return this._server;
  }
  constructor(){
    this._app = express();
    this._server = http.Server(this.app);
  }
  start(port){
    port = port || 8080;
    console.log(`server listening on ${port} port...`);
    this.server.listen(port);
    return this;
  }
  registerWebApi() {
    this.app.disable('x-powered-by');
    this.app.use(bodyParser.json());
    this.app.use(extensions);
    this.app.use(storage);
    this.app.use('/', router);

    return this;
  }
  serveStaticFile(filePath, route) {
    route = route || '/';
    if(fs.existsSync(filePath)){
      this.app.get(route, (req, res) => res.sendFile(filePath));
    } else if(filePath){
      throw new Error(`file ${filePath} not found`);
    }
    console.log(`static file {route: ${route}, path: ${filePath}}`);
    return this;
  }
  useConsoleLogger(){
    this.app.use(consoleLogger);
    return this;
  }
};


