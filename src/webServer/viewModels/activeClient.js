'use strict';

module.exports = {
  fromClientInfo(info){
    return {
      id: info.clientId,
      name: info.clientName
    };
  }
};
