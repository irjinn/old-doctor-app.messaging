'use strict';
const utils = require('../../utils');
const router = require('express').Router({ mergeParams: true });

module.exports = router;

router.route('/req')
  .get((req, res) => res.json(utils.decycle(req)));

router.route('/res')
  .get((req, res) => res.json(utils.decycle(res)));

router.route('/params/:id?')
  .get((req, res) => res.json({ params: req.params, query: req.query }));

