'use strict';
const router = require('express').Router({ mergeParams: true });
const messages = require('../../services/messages');
const vm = require('../viewModels');

module.exports = router; // doctors

router.route('/:userId/messages')
  .post((req, res) =>{
    let userId = req.params['userId'];
    let msg = req.body;
    msg.fromId = userId;
    messages.doctor.add(msg)
      .then(res.jsonSuccess);
  });



