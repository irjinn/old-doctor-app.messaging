'use strict';
const router = require('express').Router({ mergeParams: true });
const clients = require('../../services/clients');
const messages = require('../../services/messages');
const conversations = require('../../services/conversations');
const vm = require('../viewModels');

module.exports = router; // users

router.route('/:userId/messages')
  .post((req, res) =>{
    let userId = req.params['userId'];
    let msg = req.body;
    msg.fromId = userId;
    messages.patient.add(msg)
      .then(res.jsonSuccess);
  });

router.route('/online')
  .get((req, res) => {
    clients.online()
      .then(arr => arr.map(vm.activeClient.fromClientInfo))
      .then(res.jsonSuccess);
  });

//next: add viewModels
router.route('/:userId/conversations') //read=[0,1]&answered=[0,1]
  .get((req, res) =>{
    let userId = req.params['userId'];
    let query = {
      read: req.query.read,
      answered: req.query.answered,
    };
    conversations.patient.all(userId, query)
      .then(res.jsonSuccess);
  });

router.route('/:userId/conversations/:conversationId')
  .get((req, res) =>{
    let id = req.params['conversationId'];
    let userId = req.params['userId'];
    conversations.findById(id)
      .then(res.jsonSuccess);
  });


