'use strict';
const router = require('express').Router();
//todo: use view models
module.exports = router;
router.use('/units', require('./unit'));
router.use('/users', require('./users'));
router.use('/doctors', require('./doctors'));

router.use('/demo', require('./demo'));



