'use strict';
const router = require('express').Router({ mergeParams: true });
const contacts = require('../../services/contacts');
const vm = require('../viewModels');

module.exports = router; // users

router.route('/:unitId/contacts')
  .get((req, res) =>{
    let careUnitId = req.params['unitId'];
    contacts.all()
      .then(res.jsonSuccess);
  });
router.route('/:unitId/contacts')
  .post((req, res) =>{
    let careUnitId = req.params['unitId'];
    let contact = req.data;
    contacts.insert(careUnitId, contact)
      .then(res.jsonSuccess);
  });

router.route('/:unitId/contacts/online')
  .get((req, res) => {
    let careUnitId = req.params['unitId'];
    contacts.online(careUnitId)
      .then(arr => arr.map(vm.activeClient.fromClientInfo))
      .then(res.jsonSuccess);
  });



