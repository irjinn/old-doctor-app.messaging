'use strict';
const clone = require('../utils/clone');
const sanitize = require('../utils/sanitize');
const socket = require('../socketServer');
const mongo = require('../storage/mongo');

module.exports = {
  getById(id){
    return mongo.contacts.getById(id);
  },
  add(contact){
    return mongo.contacts.insert(contact);
  },
  all(){
    return mongo.contacts.all();
  },
  online(){
    //next: join active client and contact list on clientId
    return Promise.resolve(
      socket.activeClients());
  },
  setSender(msg){
    if(!msg || !msg.fromId){
      return Promise.resolve(msg);
    }
    return mongo.contacts.getById(msg.fromId)
      .then(sanitize.model)
      .then(contact => {
        if(!contact){
          return msg;
        }
        let result = clone(msg);
        result.from = contact;
        return result;
      })
  }
};
