'use strict';
const clone = require('../utils/clone');
const guid = require('../utils/guid');
const socket = require('../socketServer');
const mongo = require('../storage/mongo');
const contacts = require('./contacts');
const conversation = require('../models/conversation');

module.exports = {
  //next: validate incoming messages
  patient: {
    add(msg){
      return addMessage(msg, msg.toId, msg.fromId);
    }
  },
  doctor: {
    add(msg){
      return addMessage(msg, msg.fromId, msg.toId);
    }
  },
  messageRead(msgId){
    //todo: keep conversation => lastMessageId map
    //todo: update conversation if needed
    //todo: router.messageRead(notification)
    return mongo.messages.setIsRead(msgId);
  }
};

function addMessage(msg, doctorId, patientId){
  //next: check is msg.id exist in db
  msg.id = msg.id || guid.new();
  let query = {
    id: msg.conversationId,
    doctorId: doctorId,
    patientId: patientId,
    // lastMessageId: msg.id,
    lastMessage: msg,
  };
  return mongo.conversations.findOne(query)
    .then(conv => {
      if(!conv){
        conv = conversation.createFrom(query);
        conv.lastMessage.conversationId = conv.id;
        return mongo.conversations.insert(conv).then(() => conv);
      }
      return conv;
    })
    .then(conv => {
      msg.conversationId = conv.id;
      return mongo.messages.insert(clone(msg))
        .then(cr => msg);
    })
    .then(msg => contacts.setSender(msg))
    .then(msg => socket.postTextMessage(msg));
}