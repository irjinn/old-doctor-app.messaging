'use strict';
const socket = require('../socketServer');
const mongo = require('../storage/mongo');

module.exports = {
  online(){
    return Promise.resolve(
      socket.activeClients());
  },
  update(clientInfo){
    //todo: connection info to documentDb
    let client = {
      clientId: clientInfo.clientId,
      clientName: clientInfo.clientName,
      status: clientInfo.status,
      ip: clientInfo.ip,
    };
    return mongo.clients.upsert(client);
  },
};
