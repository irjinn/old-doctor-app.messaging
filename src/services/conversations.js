'use strict';
const mongo = require('../storage/mongo');

module.exports = { //conversations
  findById(id){
    return mongo.conversations.findById(id);
  },
  patient: {
    all(userId, query){
      return mongo.conversations.patient.all(userId, query);
    },
    details(conversationId){
      return mongo.conversations.patient.details(conversationId)
        .then(conv => {
          if(!conv) return conv;
          return mongo.messages.fromConversation(conversationId)
            .then(arr => {
              conv.messages = arr;
              return conv;
            })
        })
    },
    unread(userId){
      //todo: query patient unread conversations
      //no need for embedding doctor contact
      //will be called a lot
      return Promise.resolve([]);
    },
  },
  doctor: {
    //todo: doctor conversations operations
  }
};
