'use strict';
const path = require('path');
const config = require('../config');
const storage = require('./storage');
const WebFacade = require('./webServer');
const socketServer = require('./socketServer');

const port = process.env.PORT || config.server.port || 8080;
const indexHtml = config.server.indexHtml ||
  path.join(__dirname, '../index.html');

const web = new WebFacade();

Promise.resolve(web)
  .then(() => storage.initialize())
  .then(() => socketServer.init(web.server))
  .then(() => web.useConsoleLogger())
  .then(() => web.registerWebApi())
  .then(() => web.serveStaticFile(indexHtml, '/'))
  .then(() => web.start(port))
  ;
