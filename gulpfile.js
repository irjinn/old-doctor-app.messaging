var gulp = require('gulp');
var path = require('path');
var zip = require('gulp-zip');
var fs = require('fs');

var options = {
  packageName: "package.zip",
  packageDir: path.join(__dirname, 'bin')
};

gulp.task('default', function () {

  var packagePaths = ['**',
    '!**/_package/**',
    '!**/typings/**',
    '!typings',
    '!_package',
    '!**/local.config.json',
    '!**/demo*.js*',
    '!gulpfile.js'];

  //add exclusion patterns for all dev dependencies
  var packageJSON = JSON.parse(fs.readFileSync(path.join(__dirname, 'package.json'), 'utf8'));
  var devDeps = packageJSON.devDependencies;

  for (var propName in devDeps) {
    var excludePattern1 = "!**/node_modules/" + propName + "/**";
    var excludePattern2 = "!**/node_modules/" + propName;
    packagePaths.push(excludePattern1);
    packagePaths.push(excludePattern2);
  }
  var packageFile = path.join(options.packageDir, options.packageName);
  safeDeleteFile(packageFile);
  return gulp.src(packagePaths)
    .pipe(zip(options.packageName))
    .pipe(gulp.dest(options.packageDir));
});

function safeDeleteFile(filePath) {
  try {
    if (fs.existsSync(packageFile)) {
      fs.unlinkSync(packageFile);
    }
  }
  catch (err){
    console.warn(`error access file ${packageFile}`);
  }
}