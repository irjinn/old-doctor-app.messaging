const fetch = require('node-fetch');
const url = require('./url');

module.exports = {
  getJson(url, options){
    var opt = {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' }
    };
    Object.assign(opt, options);
    return fetch(url, opt)
      .then(res => res.json());
  },
  postJson(url, data, options){
    var opt = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' }
    };
    if(data){
      Object.assign(opt, { data: JSON.stringify(data) });
    }
    Object.assign(opt, options);
    return fetch(url, opt)
      .then(res => res.json());
  },
};
