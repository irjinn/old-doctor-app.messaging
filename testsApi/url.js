const base = require('./test.api.config.json').endpoint;

module.exports = {
  endpoint(){
    return base;
  },
  doctors:{
    messages(doctorId){
      return `${base}/doctors/${doctorId}/messages`;
    },
    conversations(doctorId){
      return `${base}/doctors/${doctorId}/conversations`;
    },
    conversation(doctorId, conversationId){
      return `${base}/doctors/${doctorId}/conversations/${conversationId}`;
    },
  },
  patients:{
    contacts(patientId){
      return `${base}/patients/${patientId}/contacts`;
    },
    messages(patientId){
      return `${base}/patients/${patientId}/messages`;
    },
    conversations(patientId){
      return `${base}/patients/${patientId}/conversations`;
    },
    conversation(patientId, conversationId){
      return `${base}/patients/${patientId}/conversations/${conversationId}`;
    },
    messageRead(patientId, messageId){
      return `${base}/patients/${patientId}/messages/${messageId}/read`;
    },
  },
};