const chai = require('chai');
const describe = require('mocha').describe;
const before = require('mocha').before;
const expect = chai.expect;
const fake = require('faker');
const request = require('./webRequest');
const urls = require('./url');
const message = require('../src/models/message');
const guid = require('../src/utils/guid');

function getDoctorId(){
  return request.getJson(urls.patients.contacts(guid.new()))
    .then(x => x.data)
    .then(x => {
      var doc = x[0];
      if(!doc){
        throw Error('Contact list is empty');
      }
      return doc.id;
    });
}

describe('api.doctor.messages', function() {
  describe('post msg', function() {
    it('creates correct conversation if not exists', function() {
      return getDoctorId()
        .then(doctorId => {
          let userId = guid.new();
          let msg = message.fake({ toId: doctorId });
          let url = urls.patients.messages(userId);
          return request.postJson(url, msg);
        })
        .then(res => {
          expect(res.success).to.equal(true);
        });
    })
  });
});
