'use strict';

const webpack = require('webpack');
const config = require('../webpack.config');

webpack(config).run((err, stat) => {
  if(err) {
    console.dir(err);
    throw new Error(err);
  }
  else {
    stat.compilation.fileDependencies.forEach(x => console.log('>> ' + x));
  }
});