'use strict';
const db = require('../src/storage/mongo');
const contactList = require('../src/models/contactList');
const config = require('../config');
const fakes = contactList.fake();

db.initialize(config.db.connectionString)
  .then(storage => storage.contacts.deleteAll())
  .then(() => Promise.all(fakes.map(x => db.contacts.insert(x))))
  .catch(err => {
    console.dir(err);
  })
  .then(() => process.exit(0), () => process.exit(1))
  ;
