'use strict';
const fs = require('fs');
const path = require('path');
const webpack = require('webpack');

const outputDir = path.join(__dirname, 'bin');
const entryPoint = './index.js';

// do not pack core node modules or installed packages
// http://jlongster.com/Backend-Apps-with-Webpack--Part-I
var nodeModules = {};
fs.readdirSync('node_modules')
  .filter(function(x) {
    return ['.bin'].indexOf(x) === -1;
  })
  .forEach(function(mod) {
    nodeModules[mod] = 'commonjs ' + mod;
  });

module.exports = {
  entry: {
    'index': entryPoint,
    'index.min': entryPoint
  },
  target: 'node',
  externals: nodeModules,
  output: {
    path: outputDir,
    filename: '[name].js'
  },
  module: {
    loaders: [
      {
        test: /.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ['es2015']
        }
      }
    ]
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
        include: /\.min\.js$/,
        minimize: true
      }),
    new webpack.BannerPlugin(
        'require("source-map-support").install();',
        { raw: true, entryOnly: false }
      )
  ],
  devtool: 'sourcemap'
};
